#ifndef INC_TLACITKA_H

#define INC_TLACITKA_H

#include <stdio.h>

int button_is_press1(void);
int button_is_press2(void);
int button_is_press3(void);
int button_is_press4(void);
int button_is_press5(void);
int button_is_press6(void);
int button_is_press7(void);
int button_is_press8(void);

#endif