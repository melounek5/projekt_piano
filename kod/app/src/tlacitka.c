#include "tlacitka.h"
#include "stm8s.h"

int button_is_press1(void)
{
    return (GPIO_ReadInputPin(GPIOE, GPIO_PIN_0));
}
int button_is_press2(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_1));
}
int button_is_press3(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_2));
}
int button_is_press4(void)
{
    return (GPIO_ReadInputPin(GPIOC, GPIO_PIN_4));
}
int button_is_press5(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_4));
}
int button_is_press6(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_5));
}
int button_is_press7(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_6));
}
int button_is_press8(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_7));
}