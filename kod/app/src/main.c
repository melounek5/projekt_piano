#include "stm8s.h"
#include "tlacitka.h"
#include "noty.h"

int noty[] = {262, 294, 330, 349, 392, 440, 494, 523};

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);             // FREQ MCU 16MHz
    GPIO_Init(GPIOB, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_LOW_SLOW); // LED
    GPIO_Init(GPIOD, GPIO_PIN_ALL, GPIO_MODE_IN_PU_NO_IT);     // BTN
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_IN_PU_NO_IT);

    TIM2_TimeBaseInit(
        TIM2_PRESCALER_256,
        62499);

    TIM2_OC2Init(
        TIM2_OCMODE_TOGGLE,
        TIM2_OUTPUTSTATE_ENABLE,
        0,
        TIM2_OCPOLARITY_HIGH);

    TIM2_Cmd(ENABLE);
    TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);

    while (1)
    {
        if (!button_is_press1())
        {
            const uint32_t F_OUT = noty[0]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_0);

            while (!button_is_press1())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press2())
        {
            const uint32_t F_OUT = noty[1]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_1);

            while (!button_is_press2())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_1);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press3())
        {
            const uint32_t F_OUT = noty[2]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_2);

            while (!button_is_press3())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_2);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press4())
        {
            const uint32_t F_OUT = noty[3]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_3);

            while (!button_is_press4())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_3);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press5())
        {
            const uint32_t F_OUT = noty[4]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_4);

            while (!button_is_press5())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_4);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press6())
        {
            const uint32_t F_OUT = noty[5]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_5);

            while (!button_is_press6())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_5);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press7())
        {
            const uint32_t F_OUT = noty[6]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_6);

            while (!button_is_press7())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_6);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
        if (!button_is_press8())
        {
            const uint32_t F_OUT = noty[7]; // Hz
            const uint32_t F_MCU = CLK_GetClockFreq();
            const uint32_t PRESCALER = 256;
            const uint16_t PERIOD = (F_MCU / (PRESCALER * F_OUT * 2)) - 1;

            TIM2_SetAutoreload(PERIOD);
            TIM2_SetCounter(0);
            TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
            GPIO_WriteHigh(GPIOB, GPIO_PIN_7);

            while (!button_is_press8())
                ;

            GPIO_WriteLow(GPIOB, GPIO_PIN_7);
            TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
        }
    }
} /* main */
