# Piano


## Základní informace

V mém projektu jsem vytvořil piano, které hraje 8 základních not se světelnou signalizací.
Jednotlivé noty hrajeme pomocí klasických tlačítek a jejich zvuk vydává reproduktor.
Buzení reproduktoru probíhá pomocí PWM. Každé tlačítko  po zmáčknutí vyšle signál do STM8, který mu nařídí, aby změnil nastavení PWM, rozsvítil patřičnou LED diodu a vyslal signál PWM do reproduktoru, který poté zahraje žádaný tón. 

## Blokové schéma

```mermaid
flowchart LR
PC ==> STM8[STM8]
BTN <--> STM8
STM8 --> LED
STM8 -- PWM --> Repro
```

## Tabulka součástek

|   Typ součástky |      Počet kusů      |  Cena/1 ks  | Cena  |
|:---------------:|:--------------------:|:-----------:|:-----:|
|    Tlačítko     |    8                 | 2,50 Kč     | 20 Kč |
|   Rezistor      |    8                 |  2 Kč       | 16 Kč |
|Reproduktor      |    1                 | 50 Kč       | 50 Kč |
| STM8 Nucleo     |    1                 | 250 Kč      | 250 Kč|
|LED dioda        |    8                 |  1 Kč       | 8 Kč  |

## Schéma v KiCadu

![](img/schema_piano.png)
- [Blok1](img/schema_piano_d.png)
- [Blok2](img/schema_piano_t_r.png)
- [Blok3](img/schema_piano_t.png)
- [PDF](Texty/mit-piano.pdf)

## Zapojení v realitě

![](img/irl.jpg)

## Zdrojový kód

- [MAIN](kod/app/src/main.c)
- [tlačítka](kod/app/src/tlacitka.c)

## Závěr
Tento projekt pro mě byla relativně velká výzva, protože s programovacím jazykem C nejsme úplně velcí kamarádi narozdíl od Pythonu. To znamenalo, že při tvorbě kódu jsem se setkal s nejedním problémem, které se mi většinou podařilo vyřešit, nebo nějak obejít. Největším problémem byl asi ten, že se mi reproduktor budil samovolně a hrál někdy velmi zajímavé tóny - nějaké alternativní hudební skupiny by ho v tomto stádiu ocenili určitě víc než kdejaký klavír. (A navíc by si díky LEDkám ani pomalu nemuseli pořizovat různé stroboskopy) Práce na projektu mě ale i přes tyto problémy docela bavila. 

## Autor
Jakub Švec
