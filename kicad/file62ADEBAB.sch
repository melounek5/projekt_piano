EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW4
U 1 1 62AEA1AD
P 5150 3800
F 0 "SW4" H 5150 3993 50  0000 C CNN
F 1 "SW_Push" H 5150 3994 50  0001 C CNN
F 2 "" H 5150 4000 50  0001 C CNN
F 3 "~" H 5150 4000 50  0001 C CNN
	1    5150 3800
	1    0    0    -1  
$EndComp
Text HLabel 5900 3800 2    50   Output ~ 0
C4
Text HLabel 4450 3800 0    50   Input ~ 0
GND
Wire Wire Line
	5900 3800 5350 3800
Wire Wire Line
	4950 3800 4450 3800
$EndSCHEMATC
