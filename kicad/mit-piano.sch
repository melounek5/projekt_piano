EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM8:STM8S208RB U1
U 1 1 62AB6827
P 5000 3600
F 0 "U1" H 5000 5581 50  0000 C CNN
F 1 "STM8S208RB" H 5000 5490 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 5050 1400 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stm8s208rb.pdf" H 4900 3600 50  0001 C CNN
	1    5000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5400 4800 5950
Wire Wire Line
	7000 2300 6600 2300
Wire Wire Line
	6600 2200 6600 2100
Wire Wire Line
	6600 2000 5600 2000
Wire Wire Line
	5600 2100 6600 2100
Connection ~ 6600 2100
Wire Wire Line
	6600 2100 6600 2000
Wire Wire Line
	6600 2200 5600 2200
Connection ~ 6600 2200
Wire Wire Line
	4800 5950 6750 5950
Wire Wire Line
	6750 2650 7000 2650
Wire Wire Line
	6600 2200 6600 2300
Wire Wire Line
	6600 2400 5600 2400
Wire Wire Line
	5600 2700 6600 2700
Wire Wire Line
	6600 2700 6600 2600
Connection ~ 6600 2400
Wire Wire Line
	6600 2600 5600 2600
Connection ~ 6600 2600
Wire Wire Line
	6600 2600 6600 2500
Wire Wire Line
	5600 2500 6600 2500
Connection ~ 6600 2500
Wire Wire Line
	6600 2500 6600 2400
Connection ~ 6600 2300
Wire Wire Line
	6600 2300 6600 2400
Wire Wire Line
	6750 2650 6750 5950
$Sheet
S 3000 2800 550  600 
U 62ACCAE9
F0 "Diody" 50
F1 "file62ACCAE8.sch" 50
F2 "B0-7" I R 3550 2950 50 
F3 "GND" O R 3550 3300 50 
$EndSheet
Wire Wire Line
	3550 3300 3650 3300
Wire Wire Line
	3650 5950 4800 5950
Connection ~ 4800 5950
Wire Wire Line
	3850 2700 4400 2700
Wire Wire Line
	3550 2950 3850 2950
Wire Wire Line
	3850 2950 3850 3000
Wire Wire Line
	3850 3400 4400 3400
Connection ~ 3850 2950
Wire Wire Line
	4400 3300 3850 3300
Connection ~ 3850 3300
Wire Wire Line
	3850 3300 3850 3400
Wire Wire Line
	3850 3200 4400 3200
Connection ~ 3850 3200
Wire Wire Line
	3850 3200 3850 3300
Wire Wire Line
	4400 3100 3850 3100
Connection ~ 3850 3100
Wire Wire Line
	3850 3100 3850 3200
Wire Wire Line
	4400 3000 3850 3000
Connection ~ 3850 3000
Wire Wire Line
	3850 3000 3850 3100
Wire Wire Line
	4400 2900 3850 2900
Wire Wire Line
	3850 2700 3850 2800
Connection ~ 3850 2900
Wire Wire Line
	3850 2900 3850 2950
Wire Wire Line
	4400 2800 3850 2800
Connection ~ 3850 2800
Wire Wire Line
	3850 2800 3850 2900
$Sheet
S 3000 3650 550  600 
U 62ADEBAC
F0 "Tlačítko" 50
F1 "file62ADEBAB.sch" 50
F2 "C4" O R 3550 3900 50 
F3 "GND" I R 3550 4150 50 
$EndSheet
$Sheet
S 7000 2150 550  600 
U 62AB8A75
F0 "Tlačítka" 50
F1 "file62AB8A74.sch" 50
F2 "D0-7" O L 7000 2300 50 
F3 "GND" I L 7000 2650 50 
$EndSheet
Wire Wire Line
	6600 2300 5600 2300
Wire Wire Line
	4400 3900 3550 3900
Wire Wire Line
	3550 4150 3650 4150
Wire Wire Line
	3650 3300 3650 4150
Connection ~ 3650 4150
Wire Wire Line
	3650 4150 3650 5950
$EndSCHEMATC
